# SAML EXTRAS

## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration

## INTRODUCTION

 - The SAML extras module allows to map user fields with simpleSAMLphp
 attributes during user authentication. Once attributes are mapped, the module
 calls the hook_simplesamlphp_auth_user_attributes to save the values into
 user fields after login.

## REQUIREMENTS

This module requires the following module:

 - simpleSAMLphp Authentication 
  <https://www.drupal.org/project/simplesamlphp_auth>
  
## INSTALLATION

 - Install as you would normally install a contributed Drupal module.

## CONFIGURATION

 - Configure this module:

   1. Enable SAML Extras module;
   2. Go to SAML Extras configuration page;
      (/admin/config/people/saml_extras)
   3. You can map user fields with SAML attributes after SAML authentication.
      You may also use custom values (like text, not related to SAML
	  attributes) or multiple values. Multiple values result from the
	  combination of several attributes (you can use special splitters: "," and
	  "\/").
	  
	  - Examples of multiple field values:
	    i) {SAMLattribute1},{SAMLattribute2}\/- results in 
	       {SAMLattribute1:value}-{SAMLattribute2:value}
	  
      ii)
         {SAMLattribute1},{SAMLattribute2}\/ {splitter1} ,{SAMLattribute3}\/ {splitter2}

     results in

     {SAMLattribute1:value} {splitter1} {SAMLattribute2:value} {splitter2} (...)
     (...) {SAMLattribute3:value}

 - Configure the user permissions in Administration » People » Permissions:

   - Access SAML extras configurations

     Users with this permission will be able to update SAML Extras
	 configurations.
