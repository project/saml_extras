/**
 * @file
 * Defines the behavior of the saml extras configuration page.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attaches the behavior of the saml extras configuration page.
   */
  Drupal.behaviors.samlExtras = {

    attach: function (context) {

      /**
       * Handler to enable/disable fields depending on the values on
       * checkboxes.
       */
      function validateCheckbox(element, checkbox1 = '', checkbox2 = '') {
        if (!element) {
          return;
        }
        var n = element.id.lastIndexOf(checkbox1);
        if (!n) {
          return;
        }
        var multiple = element.id.substring(0, n).concat(checkbox2);
        if (!multiple) {
          return;
        }
        if (element.checked) {
          $('#'.concat(multiple)).prop('disabled', true);
        } else {
          $('#'.concat(multiple)).prop('disabled', false);
        }
      }

      $('[name$="[custom_value]"]').each(function () {
        validateCheckbox(this, '-custom-value', '-multiple');
      });

      $('[name$="[custom_value]"]').click(function () {
        validateCheckbox(this, '-custom-value', '-multiple');
      });

      $('[name$="[multiple]"]').each(function () {
        validateCheckbox(this, '-multiple', '-custom-value');
      });

      $('[name$="[multiple]"]').click(function () {
        validateCheckbox(this, '-multiple', '-custom-value');
      });
    }
  };

})(jQuery, Drupal);
