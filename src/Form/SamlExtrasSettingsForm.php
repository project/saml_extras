<?php

namespace Drupal\saml_extras\Form;

use Drupal\saml_extras\SAMLExtras;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class SamlExtrasSettingsForm.
 */
class SamlExtrasSettingsForm extends ConfigFormBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'saml_extras_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['saml_extras.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $fields = SAMLExtras::getNoDefaultUserFields();
    $form = parent::buildForm($form, $form_state);

    $form['intro_text'] = [
      '#markup' => $this->t('<p>You can map user fields with SAML attributes after SAML authentication. You may also use custom values (like text, not related to SAML attributes) or multiple values. Multiple values result from the combination of several attributes (you can use special splitters: "," and "\/").<br>Examples of multiple field values:<ul><li>{SAMLattribute1},{SAMLattribute2}\/- <b>results in</b> {SAMLattribute1:value}-{SAMLattribute2:value}</li><li>{SAMLattribute1},{SAMLattribute2}\/ {splitter1} ,{SAMLattribute3}\/ {splitter2} <b>results in</b> {SAMLattribute1:value} {splitter1} {SAMLattribute2:value} {splitter2} {SAMLattribute3:value}</li></ul></p>'),
    ];
    $config = $this->config('saml_extras.settings');

    $rows = !empty($config->get('table')) ? array_merge_recursive($fields, $config->get('table')) : $fields;
    $header = [
      'user_field' => $this->t('User field'),
      'saml_attribute' => $this->t('SAML attribute'),
      'custom_value' => $this->t('Custom'),
      'multiple' => $this->t('Multiple'),
    ];
    $formTypes = [
      'user_field' => 'label',
      'saml_attribute' => 'textarea',
      'custom_value' => 'checkbox',
      'multiple' => 'checkbox',
    ];
    $form['table'] = $this->renderTable($header, $rows, $formTypes);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $table = $form_state->getValue('table');
    $this->configFactory->getEditable('saml_extras.settings')
      ->set('table', $table)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Render table markup.
   *
   * @param array $header
   *   Table header.
   * @param array $rows
   *   Table rows.
   * @param array $formTypes
   *   Table elements' form types.
   *
   * @return array
   *   Rendered table.
   */
  public function renderTable(array $header, array $rows, array $formTypes) {
    $table = [
      '#type' => 'table',
      '#header' => $header,
    ];
    $this->renderRows($table, $header, $rows, $formTypes);
    return $table;
  }

  /**
   * Render table rows markup.
   *
   * @param mixed $table
   *   Rendered table.
   * @param array $header
   *   Table header.
   * @param array $rows
   *   Table rows.
   * @param array $formTypes
   *   Table elements' form types.
   */
  public function renderRows(&$table, array $header, array $rows, array $formTypes) {
    $headerKeys = array_keys($header);
    $rowsKeys = array_keys($rows);
    for ($i = 0; $i < count($header); $i++) {
      for ($j = 0; $j < count($rows); $j++) {
        $hk = $headerKeys[$i];
        $rk = $rowsKeys[$j];
        if ($i === 0) {
          $table[$rk][$hk] = [
            '#type' => 'label',
            '#title' => $rows[$rk]['label'],
            '#title_display' => 'before',
          ];
        }
        else {
          $table[$rk][$hk] = [
            '#type' => $formTypes[$hk],
            '#default_value' => array_key_exists($hk, $rows[$rk]) ? $rows[$rk][$hk] : '',
          ];
        }
      }
    }

  }

}
