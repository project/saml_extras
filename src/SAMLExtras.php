<?php

namespace Drupal\saml_extras;

/**
 * SAMLExtras helper class.
 */
class SAMLExtras {

  /**
   * Get all user field definitions.
   *
   * @return array
   *   User field definitions.
   */
  public static function getUserFields() {
    $entityManager = \Drupal::service('entity_field.manager');
    $fields = $entityManager->getFieldDefinitions('user', 'user');
    return $fields;
  }

  /**
   * Get an array with all created user fields (those with prefix 'field_')
   *
   * @return array
   *   User fields.
   */
  public static function getNoDefaultUserFields() {
    $result = [];
    $userFields = self::getUserFields();

    foreach ($userFields as $k => $v) {
      if (strpos($k, 'field_') === 0) {
        $result[$k] = [];
        $result[$k]['label'] = $v->label();
      }
    }
    return $result;
  }

}
