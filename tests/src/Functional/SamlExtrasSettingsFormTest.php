<?php

namespace Drupal\Tests\saml_extras\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests saml extras settings form page.
 *
 * @group saml_extras
 */
class SamlExtrasSettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'saml_extras',
    'simplesamlphp_auth',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $account = $this->drupalCreateUser(['access saml extras configuration']);
    $this->drupalLogin($account);
  }

  /**
   * Test proper response in configuration page.
   */
  public function testHttpResponse() {
    // Load the settings page.
    $this->drupalGet('admin/config/people/saml_extras');
    $this->assertSession()->statusCodeEquals(200);
  }

}
